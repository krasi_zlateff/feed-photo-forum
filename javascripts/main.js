(function ($) {
    $(document).ready(function () {
        //Who likes this
        var eventTools,
            addToFavoritesCounter,
            whoLikesThis,
            shareThis,
            shareIcon;

        eventTools = $('.event-status-tools');
        addToFavoritesCounter = $(eventTools).find('.add-to-favorites span.text');
        whoLikesThis = $(eventTools).find('.who-likes-this-wrapper');
        shareIcon = $('.share-this-event-status .fa-share-alt');


        $(addToFavoritesCounter).off().on('click', function (e) {
            whoLikesThis = $(this).closest($(eventTools)).find('.who-likes-this-wrapper');
            if ($(whoLikesThis).hasClass('active')) {
                $(whoLikesThis).removeClass('active');
            } else {
                $(whoLikesThis).addClass('active');
            }
        });

        $(shareIcon).off().on('click', function (e) {
            shareThis = $(this).closest($('.share-this-event-status')).find('.share-this-wrapper');


            if ($(shareThis).hasClass('active')) {
                $(shareThis).removeClass('active');
            } else {
                $(shareThis).addClass('active');
            }
        });


        // Event status TEXT

        // Read more
        var readMore = $('.user-event-status .media .read-more');

        var eventStatusText = $(readMore).closest('.event-status-wrapper .event-status.text');

        // Get tex post content height
        var textPostContent = $(eventStatusText).find('.user-event-status .event-status-shared-content .media .media-content');

        var textPostMedia = $(eventStatusText).find('.user-event-status .event-status-shared-content .media');


        if ($(textPostMedia).hasClass('long-text')) {
            $(textPostContent).css({
                height: 30
            })
        }

        $(readMore).off('click').on('click', function (e) {
            e.preventDefault();
            if ($(this).hasClass('active')) {
                $(this).removeClass('active').text('повече');
            } else {
                $(this).addClass('active').text('по-малко');
            }

            var mediaContent = $(this).prev('.media-content');

            if ($(mediaContent).hasClass('active')) {
                $(mediaContent).removeClass('active').stop().animate({
                    height: 30
                }, 300)
            } else {
                $(mediaContent).addClass('active').stop().animate({
                    height: $(mediaContent).get(0).scrollHeight
                }, 300, function () {
                    $(this).height('auto');
                })
            }
        });

        // Show submit comment button
        var typeComment,
            postCommentButton;

        typeComment = $('.your-comment-wrapper .type-your-comment .type-comment');

        $(typeComment).focus(function () {
            postCommentButton = $(this).closest('.bottom-footer').find('.submit-comment');

            $(postCommentButton).fadeIn('fast');
        });

        // Share my status focus effect
        var typeMyStatus,
            overlayBackground,
            shareMyStatus,
            shareMyStatusImage,
            shareMyStatusHeadLabel,
            shareMyStatusContent,
            shareMyStatusFooter,
            shareMyStatusCancel;

        typeMyStatus = $('.share-my-status .type-my-status .type-status');
        shareMyStatusImage = $('.share-my-status .share-my-status-header .bottom-header .share-post-tools .share-image i.fa');
        shareMyStatusHeadLabel = $('.share-my-status .share-my-status-header .top-header .head-label');
        shareMyStatusContent = $('.share-my-status .share-my-status-content');
        shareMyStatusFooter = $('.share-my-status .share-my-status-footer');
        shareMyStatusCancel = $('.share-my-status .share-my-status-footer .cancel-post');
        overlayBackground = $('body .overlay-background');

        $(typeMyStatus).stop().on('click', function (e) {
            shareMyStatus = $(this).closest('.share-my-status');
            showFocus();
            $(shareMyStatus).addClass('focused');
            $(shareMyStatusFooter).removeClass('hidden');
        });

        $(typeMyStatus).stop().focus('click', function (e) {
            shareMyStatus = $(this).closest('.share-my-status');
            showFocus();
            $(shareMyStatus).addClass('focused');
            $(shareMyStatusFooter).removeClass('hidden');
        });

        $(shareMyStatusImage).off().on('click', function (e) {
            shareMyStatus = $(this).closest('.share-my-status');
            showFocus();
            $(shareMyStatus).addClass('focused');

            // $(shareMyStatusHeadLabel).removeClass('hidden');
            $(shareMyStatusContent).removeClass('hidden');
            $(shareMyStatusFooter).removeClass('hidden');
        });


        // Emoticons
        var emotions,
            mainEmotionsIcon,
            emotionsWrapper;

        mainEmotionsIcon = $('.tool.emotions i.fa.fa-smile-o');

        $(mainEmotionsIcon).stop().on('click', function () {
            emotions = $(this).closest($('.tool.emotions'));
            emotionsWrapper = $(emotions).find('.emotions-icons-wrapper');

            if ($(emotions).hasClass('active')) {
                $(emotions).removeClass('active');
            } else {
                $(emotions).addClass('active');
            }

        });

        var uploadNewPhoto = $('.uploaded-photo.select-new-photo .add-new');
        var upcomingEvents = $('body .upcoming-events');
        var showHideUpcomingEvents = $(upcomingEvents).find('header.ue-header');
        var upcomingEventsArrow = $(showHideUpcomingEvents).find('i.fa');
        var upcomingEventsContent = $(upcomingEvents).find('.ue-content');

        // Hide elements on outside clicking
        $(document).stop().on('click', function (e) {
            if ((!typeMyStatus.is(e.target) && typeMyStatus.has(e.target).length === 0) &&
                (!shareMyStatusImage.is(e.target) && shareMyStatusImage.has(e.target).length === 0) &&
                (!mainEmotionsIcon.is(e.target) && mainEmotionsIcon.has(e.target).length === 0) &&
                (!uploadNewPhoto.is(e.target) && uploadNewPhoto.has(e.target).length === 0)) {
                removeFocus();
                $(shareMyStatusContent).addClass('hidden');
                $(shareMyStatusFooter).addClass('hidden');
            }

            if (!mainEmotionsIcon.is(e.target) && (mainEmotionsIcon.has(e.target).length === 0)) {
                $('.tool.emotions').removeClass('active');
            }

            if (!addToFavoritesCounter.is(e.target) && (addToFavoritesCounter.has(e.target).length === 0)) {
                $(whoLikesThis).removeClass('active');
            }

            if (!shareIcon.is(e.target) && (shareIcon.has(e.target).length === 0)) {
                $(shareThis).removeClass('active');
            }

            if (!typeComment.is(e.target) && (typeComment.has(e.target).length === 0)) {
                $('.event-status .footer .bottom-footer .submit-comment').fadeOut('fast');
            }

            if (!upcomingEvents.is(e.target) && upcomingEvents.has(e.target).length === 0) {
                $(upcomingEvents).addClass('is-hidden');
                if ($(upcomingEvents).hasClass('is-hidden')) {
                    if ($(upcomingEventsArrow).hasClass('fa fa-chevron-up')) {
                        return;
                    } else {
                        $(upcomingEventsArrow).removeClass('fa fa-chevron-down');
                        $(upcomingEventsArrow).addClass('fa fa-chevron-up');
                    }
                }
            }
        });

        showFocus = function () {
            $(overlayBackground).addClass('active');
        };

        removeFocus = function () {
            $(overlayBackground).clearQueue().stop().removeClass('active');
            $('.share-my-status').removeClass('focused');
        };

        autoResizeTextArea = function (element) {
            element.style.height = "23px";
            element.style.height = (element.scrollHeight) + "px";
        };

        goBack = function () {
            window.history.back();
        };

        // Top header notifications on click
        var userAccountTool = $('.user-account-tools-wrapper .user-account-tool');
        var userNotificationsListWrapper = $(userAccountTool).find('.user-notifications-list-wrapper');

        // if the target of the click isn't the userNotificationsListWrapper nor a descendant of the userNotificationsListWrapper
        $(document).mouseup(function (e) {
            if (!userNotificationsListWrapper.is(e.target) && userNotificationsListWrapper.has(e.target).length === 0) {
                userNotificationsListWrapper.hide();
            }
        });

        $(userAccountTool).off('click').on('click', function (e) {
            e.preventDefault();
            var thisUserNotificationsListWrapper, otherUserNotificationsListWrapper;

            thisUserNotificationsListWrapper = $(this).find($(userNotificationsListWrapper));
            otherUserNotificationsListWrapper = $(userNotificationsListWrapper).not($(thisUserNotificationsListWrapper));

            if ($(thisUserNotificationsListWrapper).hasClass('active')) {
                $(thisUserNotificationsListWrapper).removeClass('active');
            } else {
                $(thisUserNotificationsListWrapper).addClass('active').show();
                $(otherUserNotificationsListWrapper).removeClass('active').hide();
            }
        });


        // Upload Photo Popup
        var uploadPhotoPopup = $('#modalUploadPhoto');

        var uploadedPhotoWrapperPopup = $(uploadPhotoPopup).find('.uploaded-photo-wrapper');
        var btnRemoveUploadedPhotoPopup = $(uploadedPhotoWrapperPopup).find('.delete-photo');

        $(btnRemoveUploadedPhotoPopup).off('click').on('click', function () {
            $(this).closest('.step-2').addClass('hidden');
            $(uploadPhotoPopup).find('.step-1').removeClass('hidden');
        });

        var seriesOfPhotos = $(uploadPhotoPopup).find('.series-of-photos');
        var sopPhotoSeriesWrapper = $(seriesOfPhotos).find('.photo-series-wrapper');
        var sopPhotoSeriesUploadedPhotoWrapper = $(sopPhotoSeriesWrapper).find('.uploaded-photo-wrapper');
        var sopRemovePhoto = $(sopPhotoSeriesUploadedPhotoWrapper).find('.delete-photo');
        var sopSelectNewPhoto = $(sopPhotoSeriesWrapper).find('.uploaded-photo-wrapper.select-new-photo');


        // remove uploaded series photo
        $(sopRemovePhoto).off('click').on('click', function () {

            $(this).closest(sopPhotoSeriesUploadedPhotoWrapper).remove();

            $(sopSelectNewPhoto).last().clone().appendTo($(sopPhotoSeriesWrapper));
        });

        // Suggest model
        $('.form-group .form-group-header a').off('click').on('click', function (e) {
            e.preventDefault();

            var thisFormGroup = $(this).closest('.form-group');
            var filterSuggest = $(thisFormGroup).find('.form-group-body .filter-option.suggest .bootstrap-select');
            var filterSuggestInput = $(thisFormGroup).find('.form-group-body .filter-option.suggest .form-control');

            if ($(this).hasClass('input')) {
                $(this).removeClass('input');
                if ($(thisFormGroup).hasClass('model')) {
                    $(this).text('Предложи нов модел');
                } else {
                    $(this).text('Предложи нова марка');
                }
                $(filterSuggest).show();
                $(filterSuggestInput).hide();
            } else {
                $(this).addClass('input');
                if ($(thisFormGroup).hasClass('model')) {
                    $(this).text('Избери модел');
                } else {
                    $(this).text('Избери марка');
                }
                $(filterSuggest).hide();
                $(filterSuggestInput).show();
            }
        });
    });
    // When window object is ready or loaded
    $(window).off('load').on('load', function () {

        //Upcoming events show/hide
        var upcomingEvents = $('body .upcoming-events');
        var showHideUpcomingEvents = $(upcomingEvents).find('header.ue-header');
        var upcomingEventsArrow = $(showHideUpcomingEvents).find('i.fa');
        var upcomingEventsContent = $(upcomingEvents).find('.ue-content');

        $(showHideUpcomingEvents).off().on('click', function (e) {
            if ($(upcomingEvents).hasClass('is-hidden')) {
                $(upcomingEvents).removeClass('is-hidden');
                $(upcomingEventsArrow).removeClass('fa fa-chevron-up');
                $(upcomingEventsArrow).addClass('fa fa-chevron-down');
            } else {
                $(upcomingEvents).addClass('is-hidden');
                $(upcomingEventsArrow).removeClass('fa fa-chevron-down');
                $(upcomingEventsArrow).addClass('fa fa-chevron-up');
            }
        });

        var uploadNewCoverPhoto = $('.btn-change-cover-photo');
        var uploadNewCoverPhotoWidth = $(uploadNewCoverPhoto).outerWidth();
        var uploadNewCoverPhotoHeight = $(uploadNewCoverPhoto).outerHeight();
        var uploadCoverPhotoWrapper = $(uploadNewCoverPhoto).find('#upload-cover-photo');

        $(uploadCoverPhotoWrapper).css('width', uploadNewCoverPhotoWidth);
        $(uploadCoverPhotoWrapper).css('height', uploadNewCoverPhotoHeight);

        // Profile menu
        var profileMenu = $('.profile-menu');
        var profileMenuElement = $(profileMenu).find('.menu-element');
        var profileMenuWidth = $(profileMenu).outerWidth();
        var profileMenuPosition = parseInt($(profileMenu).css("right"));
        var profileMenuClose = $(profileMenu).find('.close-menu');
        var profileMenuCloseWidth = $(profileMenuClose).outerWidth();
        var profileName = $('header.top-header .account-wrapper .user-account.logged .top-menu .menu-item .menu-element.profile-name');
        var profilePhotoImage = $(profileName).find('.profile-photo');
        var profileNameText = $(profileName).find('span.text');

        $(profileMenu).css('width', profileMenuWidth + profileMenuCloseWidth);
        $(profileMenu).css('right', -profileMenuWidth - profileMenuCloseWidth);
        $(profileMenu).css('display', 'block');

        // Open Profile menu
        $(profileName).off('click').on('click', function (e) {
            e.preventDefault();
            if (parseInt($(topMobileMenuWrapper).css('left')) === 0) {
                $(topMobileMenuWrapper).stop().animate({
                    left: -topMobileMenuWrapperWidth
                }, 300);
            }
            if (profileMenuPosition < 0) {
                $(profileMenu).stop().animate({
                    right: 0
                }, 300);
            }
        });

        // Close Profile menu
        $(profileMenuClose).off('click').on('click', function (e) {
            e.preventDefault();
            $(profileMenu).stop().animate({
                right: -profileMenuWidth - profileMenuCloseWidth
            }, 300);
        });

        // Profile menu element
        $(profileMenuElement).off('click').on('click', function (e) {
            $(profileMenu).stop().animate({
                right: -profileMenuWidth - profileMenuCloseWidth
            }, 300);
        });

        // Hide profile menu on outside clicking
        $(document).on('click', function (e) {
            if ((!profilePhotoImage.is(e.target) && profilePhotoImage.has(e.target).length === 0) &&
                (!profileNameText.is(e.target) && profileNameText.has(e.target).length === 0)) {
                $(profileMenu).stop().animate({
                    right: -profileMenuWidth - profileMenuCloseWidth
                }, 300);
            }
        });

        // Mobile menu
        var topMobileMenuWrapper = $('.top-header .mobile-menu-wrapper');
        var topMobileMenu = $(topMobileMenuWrapper).find('.top-menu');
        var topMobileMenuhasSubmenu = $(topMobileMenu).find('.menu-item.has-submenu');
        var topMobileMenuhasSubmenuMenuElement = $(topMobileMenuhasSubmenu).find('.menu-element:first');
        var topMobileMenuIcon = $('.top-header .mobile-menu .mobile-menu-hamburger');
        var topMobileMenuWrapperWidth = parseInt($(topMobileMenuWrapper).css('width'));
        var topMobileMenuWrapperPosition = parseInt($(topMobileMenuWrapper).css("left"));
        var topMobileMenuCloseButton = $(topMobileMenuWrapper).find('.mobile-menu-nav .menu-item .close-menu');
        var topMobileMenuBackButton = $(topMobileMenuWrapper).find('.top-menu .menu-item.has-submenu .submenu .back-menu-icon');

        // Open top mobile menu
        $(topMobileMenuIcon).off('click').on('click', function (e) {
            e.preventDefault();
            if (parseInt($(profileMenu).css('right')) === 0) {
                $(profileMenu).stop().animate({
                    right: -profileMenuWidth - profileMenuCloseWidth
                }, 300);
            }
            if (topMobileMenuWrapperPosition < 0) {
                $(topMobileMenuWrapper).stop().animate({
                    left: 0
                }, 300);
            }
        });

        // Close top mobile menu
        $(topMobileMenuCloseButton).off('click').on('click', function (e) {
            e.preventDefault();
            $(topMobileMenuWrapper).stop().animate({
                left: -topMobileMenuWrapperWidth
            }, 300);
        });

        // Open top mobile submenu
        $(topMobileMenuhasSubmenuMenuElement).off('click').on('click', function (e) {
            e.preventDefault();
            $(topMobileMenuBackButton).stop().animate({
                opacity: 1
            }, 300);
            var topMobileMenuSubMenu = $(this).closest($(topMobileMenuhasSubmenu)).find('.submenu:first').addClass('active');
            var topMobileMenuSubMenuPosition = parseInt($(topMobileMenuSubMenu).css('left'));
            if (topMobileMenuSubMenuPosition < 0) {
                $(topMobileMenuSubMenu).stop().animate({
                    left: 0
                }, 300);
            }
        });

        // Close top mobile submenu
        $(topMobileMenuBackButton).off('click').on('click', function (e) {
            e.preventDefault();
            var topMobileMenuSubMenu = $(topMobileMenuhasSubmenu).find('.submenu');

            if ($(topMobileMenuSubMenu).hasClass('active')) {
                var topMobileMenuSubMenuWidth = parseInt($(topMobileMenuSubMenu).css('width'));
                $(topMobileMenuSubMenu).stop().animate({
                    left: -topMobileMenuSubMenuWidth
                });
                $(topMobileMenuSubMenu).removeClass('active');
                $(this).stop().animate({
                    opacity: 0
                }, 300);
            }
        });
    });
})(jQuery);
