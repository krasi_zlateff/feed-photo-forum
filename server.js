var express = require('express');
var app = express();

// set the port of our application
// process.env.PORT lets the port be set by Heroku
var port = process.env.PORT || 8080;

// set the view engine to ejs
app.set('view engine', 'ejs');

// make express look in the public directory for env (css/js/img)
app.use(express.static(__dirname + '/public'));
app.use(express.static(__dirname + '/javascripts'));
app.use(express.static(__dirname + '/node_modules'));

// set the home page route
app.get('/', function (req, res) {
    // ejs render automatically looks in the views folder
    res.render('index');
});

// set the home page route
app.get('/post-event', function (req, res) {
    // ejs render automatically looks in the views folder
    res.render('post_event');
});

// set the user gallery page route
app.get('/gallery', function (req, res) {
    // ejs render automatically looks in the views folder
    res.render('gallery');
});

// set the user group page route
app.get('/group', function (req, res) {
    // ejs render automatically looks in the views folder
    res.render('group/group');
});

// set the user group page all members route
app.get('/group-all-members', function (req, res) {
    // ejs render automatically looks in the views folder
    res.render('group/all_members');
});

// set the user group page settings route
app.get('/group-settings', function (req, res) {
    // ejs render automatically looks in the views folder
    res.render('group/settings');
});

app.listen(port, function () {
    console.log('Our app is running on http://localhost:' + port);
});
